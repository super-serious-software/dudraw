# DUDraw

Repository for DUDraw, a fork of [Standard Draw, Draw, and DrawListener from the Princeton Standard Libraries](https://introcs.cs.princeton.edu/java/stdlib/). 

Released under the [GNU Public License version 3 (GPLv3)](https://www.gnu.org/licenses/gpl-3.0.html)


## Getting Started 

For help getting started using DUDraw, please refer to our [Wiki Pages](https://git.cs.du.edu/super-serious-software/dudraw/-/wikis/home).

If you just want the latest code, head to [Releases](https://git.cs.du.edu/super-serious-software/dudraw/-/releases)
and choose the version appropriate for your use from the latest release.
The full documentation can also be downloaded from the release page. 