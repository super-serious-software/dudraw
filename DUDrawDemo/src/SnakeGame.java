import java.awt.*;

public class SnakeGame implements DrawListener {

    enum DIRECTION {
        NORTH,
        SOUTH,
        EAST,
        WEST
    }

    private DIRECTION curDirect;
    private Draw draw;
    private player myPlayer;

    class player{
        double x,y;
        int length =1 ;

        void move(){
            switch (curDirect){

                case NORTH: y += length;
                    break;
                case SOUTH: y -= length;
                    break;
                case EAST: x += length;
                    break;
                case WEST: x -= length;
                    break;
            }
        }

    }

    SnakeGame(){
        myPlayer = new player();
        myPlayer.x = 0.5;
        myPlayer.y = 0.5;
        curDirect = DIRECTION.NORTH;
        draw = new Draw();
        draw.setFrameTime(200);
        draw.enableDoubleBuffering();
        draw.setCanvasSize(600,600);
        draw.setXscale(0, 10);
        draw.setYscale(0, 10);
        draw.addListener(this);
        // draw black grid lines with gray background


    }
    public static void main(String[] args) {
        new SnakeGame();



    }

    @Override
    public void mousePressed(double x, double y) {
//        draw.filledCircle(x,y, 0.2);
    }

    @Override
    public void mouseDragged(double x, double y) {

    }

    @Override
    public void mouseReleased(double x, double y) {

    }

    @Override
    public void mouseClicked(double x, double y) {

    }

    @Override
    public void keyTyped(char c) {
        switch (c){
            case 'w': curDirect = DIRECTION.NORTH; break;
            case 'a': curDirect = DIRECTION.WEST; break;
            case 's': curDirect = DIRECTION.SOUTH; break;
            case 'd': curDirect = DIRECTION.EAST; break;
        }
    }

    @Override
    public void keyPressed(int keycode) {

    }

    @Override
    public void keyReleased(int keycode) {

    }

    @Override
    public void update() {
        draw.clear(Color.LIGHT_GRAY);
        draw.setPenColor(Color.BLACK);
        for (int i = 0; i <= 10; i++) draw.line(i, 0, i, 10);
        for (int j = 0; j <= 10; j++) draw.line(0, j, 10, j);
        myPlayer.move();
        draw.filledSquare(myPlayer.x, myPlayer.y, 0.5);
        draw.show();
    }
}
